/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class ShooterCommand extends CommandBase {
  private Shooter m_shooter;
  private DoubleSupplier m_rightTrigger;
  /**
   * Creates a new ShooterCommand.
   */
  public ShooterCommand(Shooter shooter, DoubleSupplier rightTrigger) {
    m_rightTrigger = rightTrigger;
    m_shooter = shooter;
    addRequirements(m_shooter);
     

    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
   
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double rightTrigger = m_rightTrigger.getAsDouble();
   if(rightTrigger > 0){
    m_shooter.shootyShoot(rightTrigger);
   }else{
      m_shooter.shootyStop();
  }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
