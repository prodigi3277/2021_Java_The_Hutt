/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import java.util.concurrent.TimeUnit;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DeathWheel;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.TankDrive;

public class AutonCommand extends CommandBase {
  private final TankDrive m_tankDrive;
  private final Shooter m_shooter;
  private final DeathWheel m_DeathWheel;
  private static double AUTON_TICKS = 9501;
  private double currentTicks = 0;
  private double currentTicksTurn = 0;
  private boolean quitAuton = false;
  private boolean quitForward = false;
  private double startingTicks;
  private boolean shootAuton = true;

  /**
   * Creates a new autonCommand.
   */
  public AutonCommand(TankDrive tankDrive, Shooter shooter, DeathWheel deathWheel) {
    m_tankDrive = tankDrive;
    m_shooter = shooter;
    m_DeathWheel = deathWheel;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_tankDrive, m_shooter, m_DeathWheel);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    startingTicks = m_tankDrive.autonPathDrive();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (!quitAuton) {
      currentTicks = m_tankDrive.autonPathDrive();
    }

    if (currentTicks < startingTicks - AUTON_TICKS) {
      quitAuton =  true;
      m_tankDrive.autonPathStop();
      if(shootAuton){
        m_shooter.shootyShoot(0.4);
      }
    }

   

    //if (quitForward) {
      // m_tankDrive.autonPathTurn();
     //  currentTicksTurn = m_tankDrive.autonPathTurn();
      //m_tankDrive.autonPathStop();
   //  m_shooter.shootyShoot(0.4);
   //   m_DeathWheel.deathWheelAdvanceManually();
    // try {
  //     TimeUnit.SECONDS.sleep(10);
    //  } catch (InterruptedException e) {
        // TODO Auto-generated catch block
    //   e.printStackTrace();
    //  }
    //  m_shooter.shootyStop();

    }
   // if(currentTicksTurn > startingTicks +AUTON_TICKS ){
    //  quitAuton = true;
    //  m_tankDrive.autonPathStop();}
    
     
   // }
   // }
    
  

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_tankDrive.autonPathStop();
    m_shooter.shootyShoot(0.4);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return quitAuton;
  }
  
}
