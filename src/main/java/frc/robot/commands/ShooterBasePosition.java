// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class ShooterBasePosition extends CommandBase {
    private Shooter m_shooter;
    private boolean endCommand = false;
  public ShooterBasePosition(Shooter shooter) {
      m_shooter = shooter;
    addRequirements(m_shooter);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  
  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
      System.out.println("shooter pos changed");
      m_shooter.shooterPositionsChange();
      m_shooter.setLedStripColor();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    endCommand = m_shooter.shouldShooterStop();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_shooter.shooterUpDownStop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return endCommand;
  }
}