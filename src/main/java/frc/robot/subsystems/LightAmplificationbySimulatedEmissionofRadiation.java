/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.networktables.NetworkTable;

public class LightAmplificationbySimulatedEmissionofRadiation extends SubsystemBase {
  private final Timer timTheTimer = new Timer();
  private AddressableLED dasGummyBear;
  private AddressableLEDBuffer lightBuffer = new AddressableLEDBuffer(Constants.LED_AMOUNT);
  private NetworkTableEntry whatZoneAreWeAt;
  private NetworkTableEntry ledStripModeEntry;
  private DriverStation.Alliance alliance = DriverStation.Alliance.Red;
  private Boolean ledCycleInProgress = false;
  private int m_ledFlashCycleCount = 0;
  private int m_desiredLedFlashCycleCount = 0;
  private double desiredDelayFirstCycle = 0.0;
  private double desiredDelaySecondCycle = 0.0;

  private int zoneRed = 0;
  private int zoneBlue = 0;
  private int zoneGreen = 0;

  private DriverStation ds;

  private int m_rainbowFirstPixelHue = 0;

  public LightAmplificationbySimulatedEmissionofRadiation() {

    ds = DriverStation.getInstance();
    alliance = ds.getAlliance();
    timTheTimer.reset();
    timTheTimer.start();
    try {
      dasGummyBear = new AddressableLED(Constants.PWM_LEDS);
    } catch (Exception e) {
      Timer.delay(0.2);
      dasGummyBear = new AddressableLED(Constants.PWM_LEDS);
      System.out.println("LEDS possibly not working");
    }

    dasGummyBear.setLength(lightBuffer.getLength());
    NetworkTableInstance tableInstance = NetworkTableInstance.getDefault();
    NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);
    whatZoneAreWeAt = table.getEntry(Constants.ZONE_SHOOTER_IS_AT_KEY);
    ledStripModeEntry = table.getEntry(Constants.LED_STRIP_MODE_KEY);
  }

  private void initTimTheTimer(double desiredDelayFirstCycleLocal, double desiredDelaySecondCycleLocal,
      int desiredLedFlashCycleCount) {
    if (!ledCycleInProgress) {
      ledCycleInProgress = true;
      timTheTimer.reset();
      timTheTimer.start();
      m_desiredLedFlashCycleCount = desiredLedFlashCycleCount;
      desiredDelayFirstCycle = desiredDelayFirstCycleLocal;
      desiredDelaySecondCycle = desiredDelayFirstCycleLocal + desiredDelaySecondCycleLocal;
    }
  }

  private void setRGB(int red, int green, int blue) {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      // Sets the specified LED to the RGB values for red

      lightBuffer.setRGB(i, red, green, blue);
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  private void setBlueRgb(int index) {
    lightBuffer.setRGB(index, 0, 0, 255);
  }

  private void setRedRgb(int index) {
    lightBuffer.setRGB(index, 255, 0, 0);
  }

  private void setGreenRgb(int index) {
    lightBuffer.setRGB(index, 0, 255, 0);
  }

  private void setYellowRgb(int index) {
    lightBuffer.setRGB(index, 255, 255, 0);
  }

  private void setOffRgb(int index) {
    lightBuffer.setRGB(index, 0, 0, 0);
  }

  private void flashingColor() {
    for (var lights = 0; lights < 10; lights++) {
      setRedRgb(lights);
      // Timer.delay(0.1);
      setOffRgb(lights);
      // Timer.delay(0.1);
    }
  }

  private void setRGBReverse() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      if (i < 3) {
        setYellowRgb(i);
      } else if (i >= Constants.LED_AMOUNT - 3) {
        setGreenRgb(i);
      }
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  // https://www.youtube.com/watch?v=dQw4w9WgXcQ i think this might help

  private void setRGBForward() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      if (i < 3) {
        setGreenRgb(i);
      } else if (i >= Constants.LED_AMOUNT - 3) {
        setYellowRgb(i);
      }
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  private void setOff() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      setOffRgb(i);
    }
    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  private void setTeamColor() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      if (alliance == DriverStation.Alliance.Red)
        setRedRgb(i);
      else if (alliance == DriverStation.Alliance.Blue) {
        setBlueRgb(i);
      }
    }
    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  private void rainbow() {
    // For every pixel
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (m_rainbowFirstPixelHue + (i * 180 / lightBuffer.getLength())) % 180;
      // Set the value
      lightBuffer.setHSV(i, hue, 255, 128);
    }
    // Increase by to make the rainbow "move"
    m_rainbowFirstPixelHue += 3;
    // Check bounds
    m_rainbowFirstPixelHue %= 180;

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  private void popoMode() {
    initTimTheTimer(0.1, 0.1, 10);
    if (timTheTimer.get() < desiredDelayFirstCycle) {
      setRGB(255, 0, 0);
    }
    else
    {      
      setRGB(0, 0, 255);
    }
    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
    
    m_ledFlashCycleCount++;
    if(m_ledFlashCycleCount == m_desiredLedFlashCycleCount)
    {
      setTeamColor();
      setLedStripModeNone();
      ledCycleInProgress = false;
    }
    else
    {
      timTheTimer.reset();
      timTheTimer.start();
    }
  }

  private void zoneColors() {
    switch ((int) whatZoneAreWeAt.getDouble(Constants.ZONE_GREEN)) {
      case Constants.ZONE_GREEN:
        zoneGreen = 255;
        zoneRed = 0;
        zoneBlue = 0;
        break;
      case Constants.ZONE_BLUE:
        zoneGreen = 0;
        zoneRed = 0;
        zoneBlue = 255;
        break;
      case Constants.ZONE_YELLOW:
        zoneGreen = 255;
        zoneRed = 255;
        zoneBlue = 0;
        break;
      case Constants.ZONE_RED:
        zoneGreen = 0;
        zoneRed = 255;
        zoneBlue = 0;
        break;
    }
  }

  private void setZoneColors() {
    initTimTheTimer(0.1, 0.2, 3);

    if (timTheTimer.get() <= desiredDelayFirstCycle) {
      setOff();
    }

    if (timTheTimer.get() > desiredDelayFirstCycle && !(timTheTimer.get() > desiredDelaySecondCycle)) {
      setRGB(zoneRed, zoneGreen, zoneBlue);
    }

    if (timTheTimer.get() > desiredDelaySecondCycle) {
      setTeamColor();
      m_ledFlashCycleCount++;
      if(m_ledFlashCycleCount == m_desiredLedFlashCycleCount)
      {
        setLedStripModeNone();
        ledCycleInProgress = false;
      }
      else
      {
        timTheTimer.reset();
        timTheTimer.start();
      }
    }
  }

  private void setLedStripModeNone()
  {
    ledStripModeEntry.setDouble(Constants.LED_STRIP_MODE_NONE);
  }

  @Override
  public void periodic() {
    int ledStripModeInt = (int) ledStripModeEntry.getDouble(Constants.LED_STRIP_MODE_NONE);

    switch (ledStripModeInt) {
      case Constants.LED_STRIP_MODE_NONE:
        // Do nothing
        break;
      case Constants.LED_STRIP_MODE_POPO:
        popoMode();
        break;
      case Constants.LED_STRIP_MODE_TEAM:
        setTeamColor();
        break;
      case Constants.LED_STRIP_MODE_FORWARD:
        setRGBForward();
        break;
      case Constants.LED_STRIP_MODE_REVERSE:
        setRGBReverse();
        break;
      case Constants.LED_STRIP_MODE_SHOOTER_ZONE:
        zoneColors();
        setZoneColors();
        break;
    }
  }
}
