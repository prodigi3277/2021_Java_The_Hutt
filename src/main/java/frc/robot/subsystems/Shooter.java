/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.StatorCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.*;

public class Shooter extends SubsystemBase {
  private TalonFX rightShooterMotor;
  private TalonFX leftShooterMotor;
  private WPI_TalonSRX upDownShooterMotor;
  private static double SHOOTER_UP_DOWN_VALUE = 0.5;
  private boolean shooterUpDownToggle = false;
  private Servo cameraServo;
  private static double SERVO_UP_DEGREES = 240;
  private static double SERVO_DOWN_DEGREES = 50;
  private NetworkTableEntry ledStripModeEntry;
  private NetworkTableEntry totalBallsInRobot;
  private NetworkTableEntry whatBaseAmIAt;
  private Number MOVE_SHOOTER_WHEN_HAS_BALL = 4;
  private NetworkTableEntry areWeShooting;
  private Servo shooterAssistServo;
  private NetworkTableEntry isBallStuckInShooter;
  private NetworkTableEntry requestedZoneEntry;
  private boolean isBallStuckInShooterBoolean;
  private boolean limelightAssist = false;
  private static double SHOOTER_SPEED = 0.93;
  private double turningSpeed = 0.03;
  private double MAX_SHOOTER_UP_TICKS = 227;
  private double MIN_SHOOTER_DOWN_TICKS = 269;
  private double ZONE_TWO_TICKS = 247;
  private double ZONE_THREE_TICKS = 242;
  private double ZONE_FOUR_TICKS = 237;
  private double ZONE_FIVE_TICKS = 228;
  private int requestedZone = Constants.ZONE_GREEN;
  private double wantedTicks = ZONE_TWO_TICKS;
  private boolean shooterTravelUp = false;

  // Limelight
  private NetworkTable limeyou = NetworkTableInstance.getDefault().getTable("limelight");
  private NetworkTableEntry tv = limeyou.getEntry("tv");
  private NetworkTableEntry ty = limeyou.getEntry("ty");
  private NetworkTableEntry ledMode = limeyou.getEntry("ledMode");

  public Shooter(XboxController controller) {
    try {
      rightShooterMotor = new WPI_TalonFX(Constants.RS_TALON);
    } catch (Exception e) {
      System.out.println("RSM not working");
    }
    try {
      leftShooterMotor = new WPI_TalonFX(Constants.LS_TALON);
    } catch (Exception e) {
      System.out.println("LSM not working");
    }
    try {
      cameraServo = new Servo(Constants.CAMERA_SERVO);
    } catch (Exception e) {
      System.out.println("camera servo not working");
    }
    try {
      shooterAssistServo = new Servo(Constants.SHOOTER_ASSIST_SERVO);
    } catch (Exception e) {
      System.out.println("shooter assist servo not working");
    }

    try {
      upDownShooterMotor = new WPI_TalonSRX(Constants.UDSM_TALON);
    } catch (Exception e) {
      System.out.println("up/down shooter not working");
    }

    rightShooterMotor.configFactoryDefault();
    leftShooterMotor.configFactoryDefault();
    upDownShooterMotor.configFactoryDefault();

    rightShooterMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 150, 175, 5));
    leftShooterMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 150, 175, 5));

    rightShooterMotor.setInverted(true);
    rightShooterMotor.follow(leftShooterMotor);
    NetworkTableInstance tableInstance = NetworkTableInstance.getDefault();
    NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);
    totalBallsInRobot = table.getEntry(Constants.TOTAL_TABLE_KEY);
    areWeShooting = table.getEntry(Constants.IS_SHOOTER_SHOOTING_KEY);
    isBallStuckInShooter = table.getEntry(Constants.IS_SHOOTER_STUCK_KEY);
    requestedZoneEntry = table.getEntry(Constants.ZONE_SHOOTER_IS_AT_KEY);
    ledStripModeEntry = table.getEntry(Constants.LED_STRIP_MODE_KEY);
    final int timeoutBeyblade = 30;
    upDownShooterMotor.setSensorPhase(false);
    upDownShooterMotor.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder, 0, timeoutBeyblade);
    MAX_SHOOTER_UP_TICKS = upDownShooterMotor.getSensorCollection().getAnalogInRaw();
    ZONE_TWO_TICKS = MAX_SHOOTER_UP_TICKS + 112;
    ZONE_THREE_TICKS = MAX_SHOOTER_UP_TICKS + 27;
    ZONE_FOUR_TICKS = MAX_SHOOTER_UP_TICKS + 30;
    ZONE_FIVE_TICKS = MAX_SHOOTER_UP_TICKS + 35;
    MIN_SHOOTER_DOWN_TICKS = ZONE_TWO_TICKS + 5;
  }

  public void shootyShoot(double shooterSpeed) {
    if (shooterSpeed > 0) {
      isBallStuckInShooterBoolean = isBallStuckInShooter.getBoolean(false);

      areWeShooting.setBoolean(true);
      System.out.println("shooter enabled");
      leftShooterMotor.set(TalonFXControlMode.PercentOutput, SHOOTER_SPEED);
      cameraServo.setAngle(SERVO_UP_DEGREES);

      if (isBallStuckInShooterBoolean) {
        shooterAssistServo.setAngle(120);
      }
    }

  }

  // public void shootyShootAuto(){
  // if(totalBallsInRobot.getNumber(0) == MOVE_SHOOTER_WHEN_HAS_BALL){
  // boolean entry = true;
  // System.out.println("shooter enabled");
  // leftShooterMotor.set(TalonFXControlMode.PercentOutput, 0.88);
  // cameraServo.setAngle(SERVO_UP_DEGREES);}

  // }

  public void shootyStop() {
    areWeShooting.setBoolean(false);

    // System.out.println("shooter stopped");
    leftShooterMotor.set(TalonFXControlMode.PercentOutput, 0.0);
    cameraServo.setAngle(SERVO_DOWN_DEGREES);
  }

  public void shooterUp() {
    if (upDownShooterMotor.getSensorCollection().getAnalogInRaw() > MAX_SHOOTER_UP_TICKS) {
      upDownShooterMotor.set(SHOOTER_UP_DOWN_VALUE);
    }
  }

  public boolean shouldShooterStop() {
    boolean endCommand = false;
    if (shooterTravelUp == false && upDownShooterMotor.getSensorCollection().getAnalogInRaw() > wantedTicks) {
      shooterUpDownStop();

      endCommand = true;
    } else if (shooterTravelUp && upDownShooterMotor.getSensorCollection().getAnalogInRaw() < wantedTicks) {
      shooterUpDownStop();
      endCommand = true;
    }
    SmartDashboard.putString("shooterTravelUp", Boolean.toString(shooterTravelUp));
    SmartDashboard.putNumber("shud position", upDownShooterMotor.getSensorCollection().getAnalogInRaw());
    SmartDashboard.putNumber("wantedTicks", wantedTicks);
    SmartDashboard.putNumber("max ticks", MAX_SHOOTER_UP_TICKS);
    SmartDashboard.putNumber("min ticks", MIN_SHOOTER_DOWN_TICKS);
    return endCommand;
  }

  public void captureRequestedBase() {
    requestedZoneEntry.setDouble(requestedZone);
  }

  public void shooterPositionsChange() {

    if (requestedZone == Constants.ZONE_GREEN) {
      wantedTicks = ZONE_TWO_TICKS;
      shooterTravelUp = false;
      shooterDown();
      captureRequestedBase();
      requestedZone = Constants.ZONE_BLUE;
      System.out.println("in zone green");
    } else if (requestedZone == Constants.ZONE_BLUE) {
      wantedTicks = ZONE_THREE_TICKS;
      shooterTravelUp = true;
      shooterUp();
      System.out.println("in zone blue");
      captureRequestedBase();
      requestedZone = Constants.ZONE_YELLOW;
    } else if (requestedZone == Constants.ZONE_YELLOW) {
      wantedTicks = ZONE_FOUR_TICKS;
      shooterTravelUp = false;
      shooterDown();
      System.out.println("in zone yellow");
      captureRequestedBase();
      requestedZone = Constants.ZONE_RED;
    } else if (requestedZone == Constants.ZONE_RED) {
      wantedTicks = ZONE_FIVE_TICKS;
      shooterTravelUp = false;
      shooterDown();
      System.out.println("in zone red");
      captureRequestedBase();
      requestedZone = Constants.ZONE_GREEN;
    }
  
    SmartDashboard.putNumber("requestedZone", requestedZone);
  }

  public void setLedStripColor() {
    ledStripModeEntry.setDouble(Constants.LED_STRIP_MODE_SHOOTER_ZONE);
  }

  public void shooterDown() {
    if (upDownShooterMotor.getSensorCollection().getAnalogInRaw() < MIN_SHOOTER_DOWN_TICKS) {
      upDownShooterMotor.set(-SHOOTER_UP_DOWN_VALUE);
    }
  }

  public void shooterUpDownStop() {
    upDownShooterMotor.set(0);
  }

  public void assistToggle() {
    limelightAssist = !limelightAssist;
  }

  public void centerTarget() {
    if (limelightAssist) {

      double verticalValue = ty.getDouble(0.0);
      double isTargetThere = tv.getDouble(0.0);

      if (isTargetThere == 1) {
        if (verticalValue > 0) {
          turningSpeed = (verticalValue / 20) + 0.09;
        }
        if (verticalValue < 0) {
          turningSpeed = (verticalValue / 20) - 0.09;
        }

        if (verticalValue == 0) {
          turningSpeed = 0;
        }

        if (turningSpeed > 1) {
          turningSpeed = 1;
        }
        if (turningSpeed < -1) {
          turningSpeed = -1;
        }

        // upDownShooterMotor.set(turningSpeed);
        SmartDashboard.putNumber("shooter speed", turningSpeed);
      }
    }
  }
}
