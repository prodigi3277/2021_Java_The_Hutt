/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Climber extends SubsystemBase {
  private WPI_TalonSRX upMotor;
  private WPI_TalonSRX downMotor;
  private static double MOTOR_UP_SPEED = 1;
  private static double MOTOR_DOWN_SPEED = 1;
  private boolean isClimberActive = false;
  private boolean isDownClimberReversed = false;

  /**
   * Creates a new Climber.
   */
  public Climber() {
    try {
      upMotor = new WPI_TalonSRX(Constants.UP_CLIMB_MOTOR);
    } catch (Exception e) {
      System.out.println("climb up motor not working");
    }
    try {
      downMotor = new WPI_TalonSRX(Constants.DOWN_CLIMB_MOTOR);
    } catch (Exception e) {
      System.out.println("climb down motor not responding");
    }
  }

  public void toggleClimber()
  {
    isClimberActive = !isClimberActive;
  }

  public void isDownClimberReversed()
  {
    isDownClimberReversed = !isDownClimberReversed;
  }

  public void extendClimber()
  {
    if (isClimberActive) {
      upMotor.set(-MOTOR_UP_SPEED);
    }
  }

  public void unextendClimber()
  {
    double downSpeed = MOTOR_DOWN_SPEED;
    if (isDownClimberReversed) {
      downSpeed = -downSpeed;
    }

    downMotor.set(downSpeed);
  }

  public void stopClimber()
  {
    upMotor.set(0);
    downMotor.set(0);
  }
}
