/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.*;
import frc.robot.subsystems.*;
import frc.robot.OI;
import edu.wpi.first.wpilibj2.command.Command;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
        public final XboxController m_controller = new XboxController(Constants.XBOX_CONTROLLER);
        private final XboxController m_controller2 = new XboxController(Constants.SECONDARY_XBOX_CONTROLLER);
        public final OI m_oi = new OI(m_controller2);
        // The robot's subsystems and commands are defined here...
        private final TankDrive m_tankDrive = new TankDrive(m_oi);
        //private final MeSeeColorGood m_colorSpin = new MeSeeColorGood();
        public final LightAmplificationbySimulatedEmissionofRadiation m_lights = new LightAmplificationbySimulatedEmissionofRadiation();
        private final Feeder m_feeder = new Feeder();
        private final Shooter m_shooter = new Shooter(m_controller);
        private final DeathWheel m_beyblade = new DeathWheel();
        private final Climber m_climber = new Climber();

        private final AutonCommand m_auton = new AutonCommand(m_tankDrive, m_shooter, m_beyblade);

        // TODO: controller.setRumble(GenericHID.RumbleType.kLeftRumble, 1);

        /**
         * The container for the robot. Contains subsystems, OI devices, and commands.
         */
        public RobotContainer() {
                // Assign default commands
             //   System.out.println(m_controller.getTriggerAxis(Hand.kRight));

                m_tankDrive.setDefaultCommand(
                                new VroomVroom(m_tankDrive, () -> m_oi.getLeftY(), () -> -m_oi.getRightX()));

                m_beyblade.setDefaultCommand(new DeathWheelQueueing(m_beyblade));
                 m_feeder.setDefaultCommand(new StopFeederAutomatic(m_feeder));
                m_feeder.setDefaultCommand(new FeederBeltCommand(m_feeder));
                m_shooter.setDefaultCommand(new ShooterCommand(m_shooter, () -> m_oi.getTriggerRight()));
                // Configure the button bindings
                configureButtonBindings();
        }

        /**
         * Use this method to define your button->command mappings. Buttons can be
         * created by instantiating a {@link GenericHID} or one of its subclasses
         * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
         * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
         */
        private void configureButtonBindings() {

              //  final JoystickButton feederBarButton = new JoystickButton(m_controller,
              //                  Constants.XBOX_BUTTON.XBOX_RIGHT_SHOLDER_BUTTON.getValue());
              //  final JoystickButton deathWheelManual = new JoystickButton(m_controller,
              //                  Constants.XBOX_BUTTON.XBOX_LEFT_SHOLDER_BUTTON.getValue());
                final JoystickButton feederChangeDirectionButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_LEFT_INDEX_TRIGGER.getValue());
                //final JoystickButton colorReadButton = new JoystickButton(m_controller,
                // Constants.XBOX_BUTTON.XBOX_START_BUTTON.getValue());
                final JoystickButton finesseButton = new JoystickButton(m_controller2,
                                Constants.XBOX_BUTTON.XBOX_START_BUTTON.getValue());
                final JoystickButton reverseButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_BACK_BUTTON.getValue());
                final JoystickButton shooterUpDownButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_Y_BUTTON.getValue());
                final JoystickButton shooterDownButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_A_BUTTON.getValue());
                final JoystickButton feederUpDownButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_B_BUTTON.getValue());
                final JoystickButton climberUpButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_RIGHT_SHOLDER_BUTTON.getValue());
                final JoystickButton climberDownButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_LEFT_SHOLDER_BUTTON.getValue());
                final JoystickButton llButton = new JoystickButton(m_controller2,
                                Constants.XBOX_BUTTON.XBOX_Y_BUTTON.getValue());
                 final JoystickButton shooterChangeButton = new JoystickButton(m_controller,
                                Constants.XBOX_BUTTON.XBOX_X_BUTTON.getValue());

                boolean interruptableCommand = true;
                //feederBarButton.whenPressed(new FeederCommand(m_feeder));
                feederUpDownButton.whenPressed(new UpDownFeederCommand(m_feeder));
                climberUpButton.whenHeld(new ClimberUpCommand(m_climber));
                finesseButton.whenPressed(new FinesseModeToggle(m_tankDrive));
              //  deathWheelManual.whenPressed(new DeathWheelAdvanceManual(m_beyblade));
               //  colorReadButton.whenPressed(new ColorIdentification(m_colorSpin));
                shooterUpDownButton.whileHeld(new ShooterUpCommand(m_shooter));
                shooterDownButton.whileHeld(new ShooterDownCommand(m_shooter));
                shooterChangeButton.whenPressed(new ShooterBasePosition(m_shooter));
                feederChangeDirectionButton.whenPressed(new ChangeFeederBarDirectionCommand(m_feeder));
                reverseButton.whenHeld(new ReverseEverything(m_feeder, m_beyblade));
                climberDownButton.whenHeld(new ClimberDownCommand(m_climber));
                llButton.whileHeld(new DriveWithLimelight(m_tankDrive), interruptableCommand);
        }

        /**
         * Use this to pass the autonomous command to the main {@link Robot} class.
         *
         * @return the command to run in autonomous
         */

        public Command getAutonomousCommand() {
                return m_auton;
        }

}
