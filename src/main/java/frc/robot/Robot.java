
package frc.robot;


import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;


public class Robot extends TimedRobot {
  private Command m_autonomousCommand;
  private RobotContainer m_robotContainer;
  
  @Override
  public void robotPeriodic() {
    m_robotContainer.m_oi.updateControllers();
    m_robotContainer.m_oi.deadZone();
    CommandScheduler.getInstance().run();
  }
  
  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
  }

  @Override
  public void autonomousInit() {
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    if (m_autonomousCommand != null) {
      m_autonomousCommand.schedule();
    }
  }

  @Override
  public void autonomousPeriodic() {
  }

  @Override
  public void testInit() {
   
    CommandScheduler.getInstance().cancelAll();
  }

  @Override
  public void testPeriodic() {
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();}
  }
  @Override
  public void robotInit()
  {
    m_robotContainer = new RobotContainer();

   
  }

  @Override
  public void teleopPeriodic()
  {
    
  }
}
